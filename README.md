# readme

I'm a privacy and security researcher specializing in applied cryptography (especially end-to-end-encryption), web application security, protocol design, and peer-to-peer systems.

I can be found on the fediverse: [ansuz@social.cryptography.dog](https://social.cryptography.dog/@ansuz).